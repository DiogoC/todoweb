from datetime import datetime, timezone

from django.contrib import auth
from pyrebase import pyrebase

# this contains all logic with database
from Logic.constants import *

fire_baseConfig = {
    'apiKey': "AIzaSyCA8Z-EBk3YKcf_QS45Zk_VFR__0phbi_0",
    'authDomain': "todowebdb.firebaseapp.com",
    'databaseURL': "https://todowebdb.firebaseio.com",
    'projectId': "todowebdb",
    'storageBucket': "todowebdb.appspot.com",
    'messagingSenderId': "765502640491",
    'appId': "1:765502640491:web:f0b9be97b78a3ada8dbc2e",
    'measurementId': "G-QG1F2ED935"
}

fire_base = pyrebase.initialize_app(fire_baseConfig)
authentication = fire_base.auth()
db = fire_base.database()


def auth_routine(uid):
    acc_info = authentication.get_account_info(uid)
    acc_info = acc_info[users]
    acc_info = acc_info[0]

    return acc_info[localId]


def sign_up(username, email, password):
    try:
        user = authentication.create_user_with_email_and_password(email, password)

        if user is not None:
            data = {name: username, email_o: email}
            db.child(users).child(user[localId]).child(settings).set(data)
        return user
    except:
        return None


def sign_in(email, password):
    try:
        return authentication.sign_in_with_email_and_password(email, password)
    except:
        return None


def logout(request):
    auth.logout(request)


def get_types():
    try:
        types_list = db.child(types).shallow().get().val()

        temp = []
        for item in types_list:
            temp.append({
                type_o: item,
                'value': db.child(types).child(item).get().val()
            })

        return temp
    except:
        return None


# POST to_do
def create_to_do(to_do, date, uid, type_t):
    try:
        acc_info = auth_routine(uid)
        timestamp = datetime.now(timezone.utc)
        timestamp = timestamp.strftime("%Y-%m-%d")

        data = {
            timestamp_o: str(timestamp),
            state_o: 0,
            type_o: int(type_t)
        }

        db.child(users).child(acc_info).child(list_o).child(date).child(to_do).set(data)

        return 1
    except:
        return 0


# GET All to_do list
def get(uid):
    try:
        acc_info = auth_routine(uid)

        list_day = db.child(users).child(acc_info).child(list_o).shallow().get().val()

        list_do = []
        for day in list_day:
            list_do.append(get_by_date(uid, day))

        return list_do
    except:
        return None


# GET to_do by date
def get_by_date(uid, date):
    try:
        acc_info = auth_routine(uid)
        to_dos = db.child(users).child(acc_info).child(list_o).child(date).get().val()

        if to_dos is None:
            return None

        to_dos = to_dos.items()
        list_do = []
        for to_do in to_dos:
            list_do.append(
                {
                    to_do_o: to_do[0],
                    state_o: to_do[1][state_o] if isinstance(to_do[1][state_o], int) else int(to_do[1][state_o]),
                    type_o: to_do[1][type_o] if isinstance(to_do[1][type_o], int) else int(to_do[1][type_o])
                }
            )

        return list_do
    except:
        return 1


# POST alter to_do
def mark(uid, to_do, date):
    try:
        acc_info = auth_routine(uid)
        state = db.child(users).child(acc_info).child(list_o).child(date).child(to_do).child(state_o).get().val()

        data = {
            state_o: '1' if state == '0' else '0'
        }

        db.child(users).child(acc_info).child(list_o).child(date).child(to_do).update(data)
        return 1
    except:
        return 0


def remove_to_do(uid, to_do, date):
    try:
        acc_info = auth_routine(uid)
        db.child(users).child(acc_info).child(list_o).child(date).child(to_do).remove()

        return 1
    except:
        return 0
