from django.conf.urls import url
from django.urls import path

from Logic import views

urlpatterns = [
    path('', views.login, name='login'),
    path('about/', views.about, name='about'),
    url(r'^home(?:/(?P<timestamp>\d{4}-\d{2}-\d{2}))?/$', views.home, name='home'),
    path('register/', views.register, name='register'),
    path('logout/', views.logout, name='logout'),
    url(r'^remove/(?P<to_do>.*)/(?P<date>.*)', views.remove, name='remove'),
    url(r'^change/(?P<to_do>.*)/(?P<date>.*)/$', views.mark_check, name='change'),
    url(r'^add/(?P<date>\d{4}-\d{2}-\d{2})/$', views.add, name='add')
]
