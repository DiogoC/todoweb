from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.template.defaulttags import register
from django.urls import reverse
from django.utils import timezone

from Logic import controller
from Logic.controller import *

li_img = ['images/chill.png', 'images/panic.png', 'images/overdose.png', 'images/apocalypse.png']


@register.filter
def get_item_by_index(array, value):
    return array[value]


@register.filter
def get_item_dic(dictionary, value):
    for dic in dictionary:
        if dic['value'] == value:
            return dic['type']
    return None


@register.filter
def get_item(dictionary, key):
    return dictionary[key]


def home(request, timestamp=None):
    if timestamp is None:
        timestamp = datetime.now(timezone.utc)
        timestamp = timestamp.strftime('%Y-%m-%d')

    if uid_o not in request.session:
        return HttpResponseRedirect(reverse('login'))

    list_todo = get_by_date(request.session[uid_o], timestamp)
    if list_todo == 1:
        return HttpResponseRedirect(reverse('logout'))

    list_types = get_types()
    return render(request, 'home.html', {date_o: timestamp, list_o: list_todo, 'list_img': li_img, 'list_types': list_types})


def login(request):
    if uid_o in request.session:
        return HttpResponseRedirect(reverse('home'))

    if request.method == 'GET':
        return render(request, 'login.html')

    user = sign_in(request.POST.get(email_o), request.POST.get(password_o))

    if user is None:
        return render(request, 'login.html', {'msg': 'Invalid credentials'})

    request.session[uid_o] = str(user['idToken'])
    return HttpResponseRedirect(reverse('home'))


def register(request):
    user = sign_up(request.POST.get(username_o), request.POST.get(email_o), request.POST.get(password_o))

    return render(request, 'login.html' if user else ('register.html', {'msg': 'Invalid credentials'}))


def logout(request):
    controller.logout(request)

    return HttpResponseRedirect(reverse('login'))


def add(request, date):
    if request.method != 'POST':
        types_list = get_types()
        return render(request, 'add.html', {date_o: date, types: types_list})

    if not create_to_do(request.POST.get(to_do_o), str(date), request.session[uid_o], request.POST.get(type_o)):
        return HttpResponseRedirect(reverse('logout'))

    return HttpResponseRedirect(reverse('home', args=(date,)))


def mark_check(request, to_do, date):
    if not mark(request.session[uid_o], to_do, date):
        return HttpResponseRedirect(reverse('logout'))

    return HttpResponseRedirect(reverse('home', args=(date,)))


def remove(request, to_do, date):
    if not remove_to_do(request.session[uid_o], to_do, date):
        return HttpResponseRedirect(reverse('logout'))

    return HttpResponseRedirect(reverse('home', args=(date,)))


def about(request):
    return render(request, 'about.html')
